import React from "react";
import Task from "../taskInfoObject";

export default function RenderTodo ({ tasks, status }) {

	// This is where Andreas task-component should go
	return (
		tasks.map((task, i) =>{
			return (
				<div key={i}>
					<Task key={task.TaskID} task={task}/>
				</div>
			);
		})
	);
}